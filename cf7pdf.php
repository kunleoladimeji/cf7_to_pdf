<?php
/*
Plugin Name: Contact Form 7 mail PDF
Plugin URI: http://mycms.localhost/
Description: Generate certificate from CF7 form. Then sends to the visitor
Author: Olakunle Oladimeji
Version: 0.3b
Author URI: http://mycms.localhost/
*/
if (!defined('ABSPATH')) {
    exit;
}
//For note later, for some servers with CRON problems, add define('ALTERNATE_WP_CRON', true). And that should fix it

/**
 * Generate PDF email to client
 */
class Contact_Form_7_Send_Email
{
    protected $visitor_email;
    protected $admin_email;
    protected $visitor_name;
    protected $email_message;
    protected $pdf_url;
    protected $pdf_instance;
    public function __construct()
    {
        define('VISITOR_TRACKER_DIR_URL', untrailingslashit(plugin_dir_path(__FILE__)));
        include('includes/pdf.php');
        $this->pdf_instance = new PDF();

        //Generate the PDF on submission of form
        add_action("wpcf7_submit", array($this, "generate_pdf"), 20, 2);

        //send certificate after mail to admin sent.
        add_action("fire_email", array($this, "send_certificate"), 10, 2);

        //Log mail errors
        add_action('wp_mail_failed', 'log_mailer_errors', 10, 1);
    }

    public function generate_pdf($WPCF7_ContactForm)
    {
        //Get the current form
        $wpcf = WPCF7_ContactForm::get_current();

        //Get current submission instance
        $submission = WPCF7_Submission::get_instance();

        if ($submission) {
            //Get the posted data
            $data = $submission->get_posted_data();

            if (empty($data)) {
                return;
            }

            //Get visitor's name and send to PDF instance
            $this->pdf_instance->name = $data['your-name'];

            //Get visitor's email address and keep for additional email sending
            $this->visitor_email = $data['your-email'];

            //Generate certificate and send the email dir url back
            $this->pdf_url = $this->pdf_instance->generate_pdf();

            wp_schedule_single_event(time(), "fire_email", array($this->visitor_email, $this->pdf_url));

            return;
        }
    }

    public function send_certificate($email, $url)
    {
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail($email, "Your certificate", "Thank you for your donation! <br /><br />Please find attached your certificate.", $headers, $url);
        return;
    }

    public function log_mailer_errors()
    {
        $fn = ABSPATH . '/mail.log'; // say you've got a mail.log file in your server root
        $fp = fopen($fn, 'a');
        fputs($fp, "Mailer Error: " . $mailer->ErrorInfo ."\n");
        fclose($fp);
    }
}
new Contact_Form_7_Send_Email();
