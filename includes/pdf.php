<?php
include('fpdf/fpdf.php');
include('FPDI/fpdi.php');
/**
 * PDF class
 */
class PDF
{
    public $source;
    public $name;
    public $target_dir;
    public $pdf_size = array(296.926, 210.058);
    public $pdf_orientation = "Landscape";
    public $pdf_dimension = "mm";
    public $x_pos;
    public $y_pos;
    public function __construct($text=null)
    {
        if (isset($text)) {
            $this->name = $text;
        }
        $this->source = VISITOR_TRACKER_DIR_URL. '/assets/template_new.pdf';
        $this->target_dir = VISITOR_TRACKER_DIR_URL. '/exports';
        $this->pdf = new FPDI($this->pdf_orientation, $this->pdf_dimension, $this->pdf_size);
        $this->pdf->AddPage();
        $this->pdf->SetTextColor(0, 0, 0); // RGB
        $this->pdf->SetXY(65, 125); // X start, Y start in mm
        $this->pdf->SetFont('Arial', 'B', 26);
    }

    public function generate_pdf()
    {
        $this->pdf->setSourceFile($this->source);
        $template = $this->pdf->importPage(1);
        $this->pdf->useTemplate($template, 0, 0, 0, 0);
        $this->pdf->Write(0, $this->name);
        $target_title = str_replace(" ", "_", $this->name);
        $target_url = $this->target_dir. "/" .$target_title. ".pdf";
        $this->pdf->Output($target_url, "F");
        return realpath($target_url);
    }
}
new PDF();
