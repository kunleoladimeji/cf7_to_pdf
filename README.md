# Contact Form 7 to PDF

This Wordpress plugin takes data from a [CF7](https://wordpress.org/plugins/contact-form-7/) form, and appends the text to a PDF document. 

This proof of concept release has a set template pdf, but future release will allow for dynamically setting the source PDF through an object. Other TODOs can be found [here](#todos).   

## Thanks To
This plugin makes use of the following libraries
- [FPDI](https://www.setasign.com/products/fpdi/downloads/)
- [FPDF](http://www.fpdf.org/)

## Before and After

![before](before.png "Before Image")


![after](after.png "After Image")

## TODOs
1. Allow for dynamically setting:
    - template document
    - font name and sizes
    - the target directory for all conversions
2. Automatically detect PDF v1.5+, and convert to a lower version in order to work with the FPDI and FPDF libraries
3. Updated Documentation with available methods
